<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-bannieres
// Langue: fr
// Date: 04-01-2016 09:52:30
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// B
	'bannieres_description' => 'La balise <code>#BANNIERE</code> permet de positionner facilement cet objet dans vos squelettes. 
Plusieurs objets peuvent être placés sur la page et gérés en fonction de leur position (ex.: <code>#BANNIERE{1}</code>). 

Si vous lui faites passer des informations de localisation, vous pourrez choisir l\'objet à afficher en conséquence. 
Un lien est placé sur les images et le clic est comptabilisé. 

Enfin, la campagne s\'affiche pendant la période que vous définissez dans l\'espace privé.',
	'bannieres_slogan' => 'Gérer des campagnes d\'information composées d\'images ou d\'animations',
);
?>